(function(){
  'use strict';

  angular.module('rubiksApp', ['ngRoute', 'angularMoment', 'ui.bootstrap.showErrors', 'ngMessages'])
    .config(function($routeProvider){
      $routeProvider.when('/', {
        controller: 'HomeCtrl',
        templateUrl: 'js/components/home/home.html'
      });
    });

  angular.module('rubiksApp').controller('HomeCtrl', function($scope, $timeout) {
    $scope.timerData = {
      startTime: null,
      endTime: null,
      currentMoment: moment.utc(0),
      totalDuration: moment.duration(0),
      timerRunning: false,
      submitButtonDisabled: true,
      formInvalid: false
    }

    $scope.submit = function(form){
      $scope.$broadcast('show-errors-check-validity');

      if ($scope.attemptForm.$valid){
        // Add the time data to the submitted form
        form["startTime"] = $scope.timerData.startTime.toDate();
        form["endTime"] = $scope.timerData.endTime.toDate();

        $.ajax({
          method: "POST",
          url: "api/attempt",
          data: JSON.stringify(form),
          contentType: "application/json"
        }).done(function(data){
          $('#time-submitted').show();
          $timeout(function(){
            $('#time-submitted').hide();
          }, 5000);
        }).fail(function(){
          $('#error-alert').show();
          $timeout(function(){
            $('#error-alert').hide();
          }, 5000);
          $scope.timerData.submitFormDisabled = false;
        });
      } else {
        $scope.timerData.formInvalid = true;
      }
    }
    /* To stop the browser from scrolling down */
    $scope.keyDown = function(e){
      if (e.which == 32 && e.target.localName !== "input") {
        e.preventDefault();
      }
    }

    $scope.keyUp = function(e){
      if (e.which == 32 && e.target.localName !== "input") {
        $scope.toggleTimer();
        e.preventDefault();
      }
    }

    $scope.timerClicked = function(e){
      $scope.toggleTimer();
      e.preventDefault();
    }

    $scope.toggleTimer = function(){
      if ($scope.timerData.timerRunning) {
        $scope.timerData.timerRunning = false;
      } else {
        $scope.timerData.timerRunning = true;
        $scope.startTimer();
      }
    }

    $scope.startTimer = function(){
      $scope.timerData.submitButtonDisabled = true;
      $scope.timerData.startTime = moment();
      $scope.timerData.totalDuration = moment.duration(0);
      $scope.updateTimer();
    }

    $scope.updateTimer = function(){
      if ($scope.timerData.timerRunning){
        $scope.timerData.totalDuration.add(10, 'ms');
        $scope.timerData.currentMoment = moment.utc($scope.timerData.totalDuration.asMilliseconds());
        $timeout($scope.updateTimer, 10);
      } else {
        $scope.endTimer();
      }
    }

    $scope.endTimer = function(){
      $scope.timerData.endTime = $scope.timerData.startTime.clone();
      $scope.timerData.endTime.add($scope.timerData.totalDuration);
      $scope.timerData.timerRunning = false;
      $scope.timerData.submitButtonDisabled = false;
    }

    $scope.hide = function(e){
      $(e.target).parent().hide();
    }

    $(function(){
      $(document).keydown($scope.keyDown);
      $(document).keyup($scope.keyUp);
    });
  });
})();
